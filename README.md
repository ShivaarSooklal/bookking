**BookKing**

[![pipeline status](https://gitlab.com/team-name-colon/bookking/badges/master/pipeline.svg)](https://gitlab.com/team-name-colon/bookking/commits/master)

[![coverage report](https://gitlab.com/team-name-colon/bookking/badges/master/coverage.svg)](https://gitlab.com/team-name-colon/bookking/commits/v2.0)

The only Moodle activity plugin you will ever need for booking time slots!
